#!/usr/bin/perl -w
# train-evaluate.pl --- Script to parse with a parser and then evaluate it.
# Author: Arne Köhn <arne@arne-koehn.de>
# Created: 14 May 2009

use DBI;
use warnings;
use strict;
use Getopt::Long;
use Curses::UI;
use threads;
use threads::shared;
use Thread::Queue;
# This imports POSTaggerEvaluator::NegraProcess
#require 'negra-sentence-selector.pl';
require 'wpreader.pl';

my $corpus;
my $do_train;
my $do_evaluate;
my $parser;
my $numsentences;
my $pick_random;
my @outdircontents;

# this is not generic, but enough for our needs.
my $cdgversion = `./get-version.sh`;
chomp $cdgversion;

# if you have problems with threads, set this to 0.
# Warning: you won't get status updates until all jobs have finished!
my $use_threads = 1;

my $cui = Curses::UI->new (clear_on_exit => 1, -color_support => 1);


sub exit_dialog() {
    my $return = $cui->dialog(
        -message   => "Do you really want to quit?",
        -title     => "Are you sure???",
        -buttons   => ['yes', 'no'],
    );
    die "FIXME: Shut things down properly" if $return;
}

my $menu_sentences = [
    { -label => 'load sentences', -value => \&load_sentences},
#    { -label => 'evaluate on external file', -value => \&schedule_test_extern},
];

my $menu_parsers = [
    { -label => 'switch to main screen'},
    { -label => 'make graph', -value => \&draw_errorbars }
];


my $menu_data = [
    { -label => 'sentences', -submenu => $menu_sentences},
    { -label => 'parsers', -submenu => $menu_parsers},
];

my $menu = $cui->add(
    'menu', 'Menubar',
    -menu => $menu_data,
    -fg => 'blue'
);

my $win1 = $cui->add(
    'win1', 'Window',
    -border => 1,
    -y    => 1,
    -bfg  => 'red',
);

$cui->set_binding(sub {$menu->focus()}, "\cT");
$cui->set_binding( \&exit_dialog , "\cQ");

my $statuslabel = $win1->add(
    'statuslabel', 'Label',
    -text      => 'Hello, world!',
    -bold      => 1,
    -width     => 40,
);

my $parserbox = $win1->add(
    'parserbox', 'Listbox',
    -values   => [],
    -multi    => 1,
    -y        => 2,
    -height   => 10,
    -border   => 1,
    -title    => 'your parsers',
    -vscrollbar => 'right',
);

my $sentencebox = $win1->add(
    'sentencebox', 'Listbox',
    -values   => [],
    -multi    => 1,
    -y        => 12,
    -height   => 10,
    -border   => 1,
    -title    => 'your sentences',
    -vscrollbar => 'right',
);

my $buttonbox = $win1->add(
    'buttonbox', 'Buttonbox',
    -buttons => [
        {
            -label => 'parse',
            -onpress => \&schedule_parse,
        },
        {
            -label => 'test',
            -onpress => \&schedule_eval,
        },
        {
            -label => 'mark all',
            -onpress => \&mark_everything,
        },
        {
            -label => 'parse-postprocess-test',
            -onpress => \&do_full_run,
        },
    ],
    -y => 22,
);



my %cfg;
# taken from http://docstore.mik.ua/orelly/perl/cookbook/ch08_17.htm
open CONFIG, "train-evaluate.conf";
while (<CONFIG>) {
    chomp;                  # no newline
    s/#.*//;                # no comments
    s/^\s+//;               # no leading white
    s/\s+$//;               # no trailing white
    next unless length;     # anything left?
    my ($var, $value) = split(/\s*=\s*/, $_, 2);
    $cfg{$var} = $value;
}; 

my $debug = exists($cfg{debug});
my $outdir = $cfg{output_directory};

# Connect to the db and create the necessary tables if they don't
# exist already
my $dbh = DBI->connect("dbi:SQLite:dbname=$cfg{'dbfile'}","","");
$dbh->do('CREATE TABLE IF NOT EXISTS sentences
         (id INTEGER PRIMARY KEY ASC,
          length INTEGER,
          filename TEXT,
          md5sum TEXT,
          created_on DEFAULT CURRENT_TIMESTAMP,
          infoblob DEFAULT NULL);');

$dbh->do('CREATE TABLE IF NOT EXISTS parsed
          (id INTEGER PRIMARY KEY ASC,
           parser TEXT,
           sentence INTEGER);');
$dbh->do('CREATE TABLE IF NOT EXISTS results
           (id INTEGER PRIMARY KEY ASC,
            version   TEXT,
            parser    TEXT,
            sentence  INTEGER,
            knownl INTEGER,
            knownu INTEGER,
            knowncnt INTEGER,
            knownbase INTEGER,
            futurel INTEGER,
            futureu INTEGER,
            futurecnt INTEGER,
            futurebase INTEGER,
            virtuall INTEGER,
            virtualu INTEGER,
            virtualcnt INTEGER,
            virtualbase INTEGER,
            increment  INTEGER,
            timelimit  BOOLEAN,
            timeneeded INTEGER,
            created_on DEFAULT CURRENT_TIMESTAMP);');

$dbh->do('create index if not exists ressentincr on results (sentence, increment);');

$dbh->do("PRAGMA synchronous=1;PRAGMA journal_mode = TRUNCATE");
my @parsers;
@parsers=split(/,/,$cfg{'parsers'});

## BEGIN JOBMANAGEMENT
my $num_running :shared = 0;
my $pending_jobs = Thread::Queue->new();
my $job_results  = Thread::Queue->new();
my $free_hosts   = Thread::Queue->new();

# We keep track of all threads to join them later.
my %threadlist;
my $threadnum = 0;

sub manage_jobs {
    my $num_pending = $pending_jobs->pending();
    while ( $num_pending and $free_hosts->pending()) {
        my $host = $free_hosts->dequeue();
        my $job = $pending_jobs->dequeue();
	my $func = \&run_parser;
	if ( $job->{'type'} eq 'eval') {
	    $func = \&eval_parser;
	}
	if ($use_threads) {
	    my $t = threads->create($func, $job->{'parser'},
				    $job->{'sentence'}, $job->{'sentenceid'},
				    $host,$threadnum);
	    if ( $t ) {
		$threadlist{$threadnum}=$t;
		$threadnum += 1;
	    }
	    else { # Thread creation failed :-(
		print STDERR "Couldn't create thread for $host\n";
		$free_hosts->enqueue($host);
		$pending_jobs->enqueue($job);
	    }
	    $num_running++;
	    $num_pending = $pending_jobs->pending();
	}
	else {
	    &$func($job->{'parser'}, $job->{'sentence'}, $job->{'sentenceid'},
		   'local', $threadnum);
	    $free_hosts->enqueue($host);
	}
    }
    if ( $num_pending or $num_running) {
        $statuslabel->text("$num_running Jobs running, $num_pending Jobs pending,");
    } else {
        $statuslabel->text("No jobs running...");
    }
    $dbh->do("BEGIN TRANSACTION;");
    while ( $job_results->pending()) {
        my $job = $job_results->dequeue();
        process_jobresult($job);
    }
    $dbh->do("COMMIT TRANSACTION");
}

sub settimeinfo {
    # gets the timing info and stores it in the db
    my $parser = shift;
    my $sentenceid = shift;
    my $foo = wpreader::read_time_information("/$outdir/wp-$sentenceid-$parser.cdp");
    if (not  $foo) {
	print STDERR "\ncouldn' find time for /$outdir/wp-$sentenceid-$parser.cdp\n\n";
	return; # nothing found, nothing to do
    }
    foreach my $r (@$foo) {
	$dbh->do(
	"UPDATE results
                   set timelimit = '" . $r->{'timelimit'} . "', timeneeded='" . $r->{'time'} . "'
                 WHERE sentence=$sentenceid AND parser='$parser' AND increment=" . $r->{'increment'} . ";"
         );
    }
}

sub process_eval_result {
    my $job = shift;
    my $parser = $job->{parser};
    my $sentence = $job->{sentenceid};
    my $table = 'results';
    my $info = $job->{'info'};
    print STDERR "got results: \n------------\n$info\n" if $debug;
    my @infos = split /\n/, $info;
    my $mode = 0;
    my $increment = -1;
    my %result = ();
    my @temp;
    sub insert2db {
	my $pars = shift;
	my $sent = shift;
	my $ref = shift;
	my %res = %$ref;
	# check if we have good data
	return if not defined($res{increment});
	my $sth = $dbh->prepare_cached("INSERT INTO results
                      (parser, version, sentence,
                       knownl, knownu, knowncnt, knownbase,
                       virtuall, virtualu, virtualcnt, virtualbase,
                       futurel, futureu, futurecnt, futurebase,
                       increment)
                       VALUES
                       (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	$sth->execute($pars, $cdgversion, $sent,
                       $res{knownl}, $res{knownu}, $res{knowncnt}, $res{knownbase},
                       $res{virtuall}, $res{virtualu}, $res{virtualcnt}, $res{virtualbase},
                       $res{futurel}, $res{futureu}, $res{futurecnt}, $res{futurebase},
                      $res{increment});
	$sth->finish();
#	print STDERR "putting in db:", $pars, $sent, $res{LABELLED}, $res{UNLABELLED}, $res{increment} if $debug;
    }
    foreach my $line (@infos) {
	chomp $line;
	if ($line eq "") {
            $mode = 0;
            insert2db($parser, $sentence, \%result);
            next;
	}
	if ($mode) {
            @temp = split / /, $line;
            $result{$temp[0]} = $temp[1];
            next;
	}
	%result = ();
	$result{file} = $line;
	$mode = 1;
    }
    # put the last entry in the db
    insert2db($parser, $sentence, \%result);
    settimeinfo($parser, $sentence);
}

sub process_jobresult {
    my $job = shift;
    $free_hosts->enqueue($job->{'host'});
    $threadlist{$job->{'clientdata'}}->join();
    $num_running--;
    my $type = $job->{'type'};
    my $parser = $job->{'parser'};
    my $sentence   = $job->{'sentenceid'};
    print STDERR "finished $type $parser \n\n" if $debug;
    if ( $type eq 'parse' ) {
        $dbh->do("INSERT INTO parsed (parser, sentence) VALUES ('$parser', $sentence);");
    } 
    elsif ( $type eq 'eval' ) {
	process_eval_result($job);
      }
    die "negative running jobs?!" if ( $num_running<0);    
}

$cui->set_timer('manage_jobs',\&manage_jobs,1);
## END JOBMANAGEMENT



$parserbox->values(@parsers);
my @hosts = split(/,/,$cfg{'hosts'});
foreach my $host (@hosts) {
    my @temp = split(/\//,$host);
    my $num = 1;
    my $hn = $host;
    # check if we want to start more than one process per machine
    if ($#temp) {
	$num = int($temp[1]);
	$hn = $temp[0];
    }
    my $i;
    for ($i=1; $i<=$num; $i++) {
	$free_hosts->enqueue($hn);
    }
}

sub update_sentencebox {
    my $sth = $dbh->prepare('SELECT * FROM sentences ORDER BY length');
    my %sentencehash;
    my @keys;
    $sth->execute();
    while (my $result = $sth->fetchrow_hashref) {
        my $id = $result->{'id'};
        push @keys, $id;
        $sentencehash{$id}=$result->{'length'}.' - '.$id;
    }
    $sentencebox->values(@keys);
    $sentencebox->labels(\%sentencehash);
}

update_sentencebox();

sub schedule_parse {
    foreach my $parser ($parserbox->get() ) {
        foreach my $sentenceid ($sentencebox->get()) {
            if ( ! $dbh->selectrow_array("SELECT id FROM parsed
                                        WHERE parser='$parser' AND sentence=$sentenceid;"))
              {
                my @res = $dbh->selectrow_array("SELECT filename FROM sentences where id=$sentenceid");
                my $sentence = $res[0];
                $pending_jobs->enqueue({'parser'=>$parser, 'sentence'=>$sentence,
                                        'sentenceid'=> $sentenceid, 'type'=>'parse'});
            }
        }
    }
    manage_jobs();
}

sub schedule_eval {
    # first, cache the contents of outdir, so we don't have to ls in every run
    @outdircontents = `ls $outdir`;
    foreach my $parser ($parserbox->get() ) {
        foreach my $sentence ($sentencebox->get()) {
            if ( ! ($dbh->selectrow_array("SELECT sentences.filename FROM results,
                                          sentences WHERE
                                          results.parser='$parser' and
                                          results.sentence='$sentence' and
                                          sentences.id='$sentence';")))
              {
                my @res = $dbh->selectrow_array("SELECT filename FROM sentences
                                                 WHERE id='$sentence';");
                $pending_jobs->enqueue({'parser'=>$parser,
                                        'sentenceid'=>$sentence,
                                        'sentence'=>$res[0],
                                        'type'=>'eval'});
            }
        }
    }
    manage_jobs();
}

sub mark_everything {
	for (0 .. $parserbox->number_of_lines() ) {
		print STDERR $_;
		$parserbox->set_selection($_);
	}
	for (0 .. $sentencebox->number_of_lines() ) {
		print STDERR $_;
		$sentencebox->set_selection($_);
	}
	$cui->draw();
}

sub do_full_run {
	schedule_parse();
	if (defined($cfg{"postparsehook"})) {
		`$cfg{"postparsehook"}`;
	}
	schedule_eval();
	if (defined($cfg{"postparsehook"})) {
		`$cfg{"posttesthook"}`;
	}
}

sub run_parser {
    my $parser = shift;
    my $sentence = shift;
    my $sentenceid = shift;
    my $host = shift;
    my $clientdata = shift;
    my $pwd = `pwd`;
    chomp $pwd;
    # replace filename with actual sentence
    $sentence = `$pwd/cda_eval/getsentence.py $sentence`;
    chomp $sentence;
    # escape all dangerous chars. Using ssh forces us to do this
    # because it uses "bash -c" to run our command and there is
    # nothing we can do about it.
    $sentence =~ s/([;<>\*\|`&\$!#\(\)\[\]\{\}:?'"])/\\$1/g;
    # use 
    my @command = ("$pwd/parsers/$parser/parse", $sentenceid, $outdir, $sentence, ">/dev/null");
    if ( not ($host eq 'local')) {
      unshift(@command, $host);
      unshift(@command, "ssh");
      system(@command);
    }
    else {
	# join the command so system uses bash. This way our sentence
	# is de-escaped the same way as with ssh.
	system(join(" ",@command));
    }
    print STDERR "\nrunning parser ",@command, "\n\n" if $debug;
    $job_results->enqueue({'type'=>'parse','parser'=>$parser,
                           'sentenceid'=>$sentenceid,'sentence'=> $sentence,
                           'host'=> $host, 'clientdata'=>$clientdata});
}

sub eval_parser {
    my $parser = shift;
    my $sentence = shift;
    my $sentenceid = shift;
    my $host = shift;
    my $clientdata = shift;
    my $pwd = `pwd`;
    chomp $pwd;
    my @parseroutfiles = grep(/^$sentenceid-$parser-/, @outdircontents);
    chomp @parseroutfiles;
    my @evalargs = ();
    foreach (@parseroutfiles) {
      push @evalargs,"$outdir/$_ $sentence";
    }
    my $evalargs = join " ", @evalargs;
    my $command = "$pwd/cda_eval/cdgevaluator.py $evalargs";
    if ( not ($host eq 'local')) {
        $command ="ssh $host $command";
    }
    print STDERR "running eval ",$command, "\n" if $debug;
    my $info = `$command 2>$outdir/evallog-$sentenceid-$parser`;
    $job_results->enqueue({'type'=>'eval',
                           'parser'=>$parser,
                           'sentence'=> $sentence,
                           'sentenceid' => $sentenceid,
                           'host'=> $host,
                           'info'=>$info,
			   'clientdata'=>$clientdata,
                       });
}

sub load_sentences {
  my $sentenceglob = $cui->question(-question=>'Where are the sentences located (glob)?');
  chomp($sentenceglob);
  my @sentences = `md5sum $sentenceglob`;
  for my $sentmd5 (@sentences) {
    my @temp = split(/ +/, $sentmd5);
    my $sentence = $temp[1];
    chomp($sentence);
    my $length = `grep ^, ${sentence} | wc -l`;
    if (not $length) {next};
    $length = $length + 1;
    my $md5 = $temp[0];
    if ($dbh->selectrow_array("SELECT id from sentences where md5sum='$md5';")) {next};
    $dbh->do("INSERT INTO sentences
                  (filename, md5sum, length) VALUES
                ('$sentence','$md5', '$length');");
  }
  update_sentencebox();
  $sentencebox->set_selection(@{$sentencebox->values()});
}


### BEGIN DRAWING FOO

sub draw_errorbars {
    my @parsers = $parserbox->get();
    my $dir = $cui->question(-question=>'Output directory of the graph?');
    my $table = $cui->question(-question=>'which table to plot?');
    my $column = $cui->question(-question=>'which column to plot?');
    if ( $column eq '' ) {
        $column = 'result';
    }
    if ( $table eq '') {
	$table = 'results';
    }
    `rm -rf graphs/$dir`;
    `mkdir graphs/$dir`;
    open my $gnuplotfile, ">", "graphs/$dir/plot";
    print $gnuplotfile "plot ";
    my $res;
    my $set_comma = 0;
    my $foobar = "";
    for my $parser (@parsers) {
	$res = $dbh->selectall_arrayref(        
	            "SELECT
                        numtrain,
                        AVG($column) AS avg,
                        MIN($column) AS min,
                        MAX($column) AS max
                     FROM
                        $table, sentences
                     WHERE
                        $table.sentence=sentences.id
                        AND parser='$parser'
                     GROUP BY numtrain
                     ORDER BY numtrain;",
               );
        open my $f, ">", "graphs/$dir/$parser";
        for my $r (@$res) {
            print $f join " ",@$r;
            print $f "\n";
        }
        if ($set_comma) {
            print $gnuplotfile ", ";
        }
        $set_comma = 1;
        print $gnuplotfile "\"$parser\" with errorbars";
    }
    print $gnuplotfile "\n";
    `cd graphs/$dir; gnuplot -persist plot`
}

$cui->mainloop;

__END__

=head1 NAME

train-evaluate.pl - train and evaluate parsers.

=head1 SYNOPSIS

perl train-evaluate.pl

=head1 DESCRIPTION

start it, use ctrl-t to open the menu, use ctrl-q to quit.

=head1 AUTHOR

Arne Köhn, E<lt>arne@arne-koehn.eeE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009 Arne Köhn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 BUGS

Curses::UI sometimes segfaults - doesn't seem to be my fault and it's not really problematic.

=cut
