#! /usr/bin/env python
# -*- coding:utf-8 -*-
# An evaluator for cda files without good documentation
# Copyright (C) 2010  Niels Beuck
# Copyright (C) 2010  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# call example: 
# parseeval/cda_eval/cdgevaluator_timecourse.py "material/cdgout/(1)-aclbase-(2).cda" "material/corpora/remixed2-negra/negra-s(1).cda" 1 500 5 5


from cda_parse.cdgoutparse import parser, CdaSpec, CdaWord, ParseException

import sys
import copy
import traceback

from optparse import OptionParser
optparser = OptionParser()
optparser.add_option("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="print infos about mappings etc.")
optparser.add_option("-m", "--malt",
                  action="store_true", dest="malt", default=False,
                  help="expect malt style prefix files, like starting with 1 and an extra final result")
optparser.add_option("-r", "--margin",
                  action="store_true", dest="margin", default=False,
                  help="add extra prefixes in the beginning and the end to assure the same number of counts for every word")
optparser.add_option("-s", "--structural",
                  action="store_true", dest="structural", default=False,
                  help="evaluate structural prediction, i.e. virtual nodes")
optparser.add_option("-n", "--splitnonspec",
                  action="store_true", dest="splitnonspec", default=False,
                  help="handle nonspec as several virtual nodes, instead of one") 
                        # both is wrong, but we have to decide in structural mode

(options, args) = optparser.parse_args()
verbose        = options.verbose
malt           = options.malt
margin         = options.margin
structural     = options.structural
splitnonspec   = options.splitnonspec

if malt: 
    sys.stderr.write("Malt mode\n")

if len(args) == 0:
    sys.stderr.write("""usage: cdgevaluator.py test gold minId maxId nback nforw\n""")
    exit
if (len(args)) < 6:
    sys.stderr.write("""you must specify 6 parameters!\n""")
    exit

la = 0 # lookahead
if len(args) >= 7:
    la = int(args[6])


def better(score1, score2):
    if score1[0] < score2[0] or ( score1[0] == score2[0] and score1[1] < score2[1] ):
        return True
    return False

def prepareNonspec(test):
    ns_id = -1

    for n in xrange(len(test)):
        tword= test[n]
        if malt:    
            wnum =n+1 # off by one between array indices and cdg counting scheme
        else: 
            wnum = n

        tp= tword.specs["SYN"].pointer
        tv= tword.specs["SYN"].value

        if malt:    
            # in malt output nonspec attachments appear as nil attachments with the 'null' label
            # we have to change the attachments accordingly
            if tv=='null' and tp== 0:
                if tword.specs["cat"].value == '$.' or tword.specs["cat"].value == '$,' or tword.specs["cat"].value == '$.(' :
                    tword.specs["SYN"].value   = ''
                else:
                    tword.specs["SYN"].pointer = ns_id
                    if splitnonspec:
                        ns_id -= 1
        elif splitnonspec:
            # we still need to change -1 counters
            if tp == -1:
                tword.specs["SYN"].pointer = ns_id
                ns_id -= 1


# TODO 

# - anderes la und margin handling verwerfen
# - dafür sorgen, dass das aktuelle wort richtig gesetzt ist 

# makes a list of prefixes that inlcues margin and la handling
def preparePrefixList(truePrefixes, final, nback, nforw, la):
    result = []
    n = la
        
    # add empty prefixes in the beginning
    # needed for margin handling and for lookahead
    for i in xrange(la):
        result.append([])
    if margin:
        for i in xrange(nforw):
            result.append([])

    # now add the regular prefixes, but ommit the last ones, depending on the lookahead
    for i in xrange(len(truePrefixes) - la):
        result.append(truePrefixes[i])

    # add the final result once
    result.append(final)

    # if there is a margin handling we need to fill up some more with 
    if margin:
        for i in xrange( nback ):
            result.append(final)

    return result


# print all dependency edges in the annotation
def printAnno(anno):
    i=1
    for word in anno:
        print i, word.word, "--", word.specs["SYN"].value,"->", word.specs["SYN"].pointer
        i+=1

# evaluates the test annoation by comparison to the gold annotation
# virtual words are mapped by vm
def evalVM(vm, test, gold):
    #print "==============="
    #print "called: evalVM"
    #print "vm: ", vm
    errors= [0,0]
    for n in xrange(len(test)):
        tword= test[n]
        wnum =n+1 # off by one between array indices and cdg counting scheme
        tp= tword.specs["SYN"].pointer
        tv= tword.specs["SYN"].value
        
        if wnum in vm.keys():
            # dependent virtual
            
            if vm[wnum] == -1:
                # cannot be mapped                    
                errors[0] += 1
                errors[1] += 1
                continue
            
            else:
                gword= gold[vm[wnum]-1] # wnum instead of n, as we first have to look it up, 
                                        # then subtract 1
            
        elif "virtual" in tword.specs:
            # virtual but not in virtmap, thus unused, skip it
            continue
        else:
            # print "n", n
            gword= gold[n]

        gp= gword.specs["SYN"].pointer
        gv= gword.specs["SYN"].value
        if tp in vm.keys():
            # regent virtual
            
            if vm[tp] == -1:
                # cannot be mapped
                errors[0]+=1
                errors[1]+=1
                continue
                    
            else:
                # can be mapped, map it
                tp= vm[tp]

        # independent of whether it has been mapped or not, compare it
        if gp == tp:
            if not gv == tv:
                # label is wrong
                errors[0]+=1
            # else: all is well
        else:
            # attachment is wrong
            errors[0]+=1
            errors[1]+=1

    # end for n in xrange...
    
    return errors                    
# end def evalVM



# returns a mapping from virtual words in test to out-of-prefix words in gold
# the best possible mapping wrt attachment score is returned
def bestVirtMap(test, gold):
    # first get the index of the last non-virtual node
    lastnonvirt = 0 # in cdg space, where 0 = nil, not array space
    for i in xrange(len(test)):
        if 'virtual' in test[i].specs:
            break
        lastnonvirt += 1
    # lastnonvirt -= 1  # not needed, off by one error compensates
    if verbose:
        sys.stderr.write("------------------------")
        sys.stderr.write("lastnonvirt: "+ lastnonvirt)

    # calculate optimal mapping from virtual nodes to upcoming words
    virtmap= {}
    candidates = {}
    
    # initialize candidates
    #for n in xrange(lastnonvirt,len(test)):
    #    wnum= n+1
    #    candidates[wnum] = []
    #candidates[-1]= []
 

    # collect candidates
    # virtual node as regent
    for n in xrange(lastnonvirt):
        tword= test[n]
        if n >= len(gold):
            sys.stderr.write("WARNING: word "+tword.word+ " not in annotation\n")
        gword= gold[n]
        gp= gword.specs["SYN"].pointer
        wnum= n+1
        tp= tword.specs["SYN"].pointer 
        #print "tp:", tp, "len(candidates):", len(candidates),"candidates[tp]:", candidates[tp]
        #print "gword:", gword, "gwordpointer:", gword.specs["SYN"].pointer
        if tp > lastnonvirt or tp <= -1 :
            if not tp in candidates.keys():
                candidates[tp]= [-1]
            if gp> lastnonvirt :
                candidates[tp].append(gp)

    # virtual node as dependent
    for n in xrange(lastnonvirt,len(test)):
        tword= test[n]
        wnum= n+1
        tp = tword.specs["SYN"].pointer
        if tp <= lastnonvirt and (tp != 0 or tword.specs["SYN"].value != ""):
            #print wnum, "--", tword.specs["SYN"].value,"-> ", tp 
            # check possible dependents
            for m in xrange(lastnonvirt, len(gold)):
                gword= gold[m]
                wnum2= m+1
                if gword.specs["SYN"].pointer == tword.specs["SYN"].pointer:
                    if not wnum in candidates.keys():
                        candidates[wnum]= [-1]
                    if wnum2> lastnonvirt :
                        candidates[wnum].append(wnum2)

    # handle purely virtual dependents
    for n in xrange(lastnonvirt,len(test)):
        tword= test[n]
        wnum= n+1
        tp= tword.specs["SYN"].pointer
        if tp > lastnonvirt or tp <= -1 :
            if not tp in candidates.keys():
                candidates[tp]= [-1]
            if not wnum in candidates.keys():
                candidates[wnum]= [-1]
                # no use to go on, as there are no candidates, continue 
                continue
                
            # add new candidates for the regent
            for cand in candidates[wnum]:
                if cand != -1:
                    cword = gold[cand-1]
                    cp = cword.specs["SYN"].pointer
                    if cp > lastnonvirt and not cp in candidates[tp]:
                        candidates[tp].append(cp)
            
            # for all candidates of its regent
            for cand in candidates[tp]:
                for m in xrange(lastnonvirt,len(gold)):
                    gword= gold[m]
                    wnum2= m+1
                    if gword.specs["SYN"].pointer == cand:
                        if not wnum in candidates.keys():
                            candidates[wnum]= [-1]
                        if wnum2 > lastnonvirt:
                            candidates[wnum].append(wnum2)

    if verbose:
        sys.stderr.write("candidates: "+ candidates + "\n")

    # now consolidate the candidates to one interpretation

    # first simply make sets
    for key in candidates.keys():
        candidates[key]= list(set(candidates[key]))

    # we need to know if there are contested target nodes
    targets= {}
    for key in candidates.keys():
        for cand in candidates[key]:
            if not cand in targets.keys():
                targets[cand]= []
            targets[cand].append(key)

    for key in targets.keys():
        targets[key]= list(set(targets[key]))
        
    # assign all unique matchings
    remaining={}
    for key in candidates.keys():
        if len(candidates[key]) == 1 and len(targets[candidates[key][0]]) == 1 : 
            virtmap[key]= candidates[key][0]
        else:
            remaining[key]= candidates[key]
            
    # now let the fun part begin: try out all combinations and keep the best one
    # [0: vmc, 1: list of occupied candidates]
    #vmcs = map(lambda x: [x, []],copy.copy(virtmap))
    vmcs = [[copy.copy(virtmap), []]]
    for key in remaining.keys():
        new_vmcs = []
        # print "vmcs: ", vmcs
        for vmc in vmcs:
            # print key, "from", remaining
            for cand in remaining[key]:
                #print "cand:", cand
                #print "vmc[1]:", vmc[1]
                #print "vmc:", vmc
                if not(cand in vmc[1]):
                    new_vmc= copy.copy(vmc[0])
                    new_vmc[key]= cand
                    new_vmc1=copy.copy(vmc[1])
                    new_vmc1.append(cand)
                    new_vmcs.append([ new_vmc, new_vmc1 ])
            new_vmc= copy.copy(vmc[0])
            # print "key:", key, "new_vmc:", new_vmc
            new_vmc[key]= -1 # no mapping
            new_vmcs.append([ new_vmc , copy.copy(vmc[1]) ])
        vmcs = new_vmcs

    # search for the best one
    best = [1000, 1000] # [labelled, unlabelled]
    best_vm = {}
    if(len(vmcs) == 0):
        sys.stderr.write("ERROR, no mapping found!\n")
        # TODO: What happens after this error?
    for vmcl in vmcs:
        vmc= vmcl[0]
        score= evalVM(vmc, test, gold)
        if better(score, best):
            best_vm= vmc
            best=score

    virtmap= best_vm
    if verbose:
        sys.stderr.write("selected mapping: " + virtmap + "\n")
    
    return virtmap

# used to measure the connectedness
# expect there to be no cycles
# simply count the words attached to nil or nonspec 
def countRoots(test):
    roots = 0
    for i in xrange(len(test)):
        tword = test[i]
        tp= tword.specs["SYN"].pointer
        tv= tword.specs["SYN"].value
        if tp < 0 or (tp == 0 and not tv == "") or (not structural and i>getLastNonVirt(test)-1 and not tv == ""):
            roots +=1
    if roots > 0 :
        return roots - 1
    else:
        return 0

# in cdg space, where 0 = nil, not array space
def getLastNonVirt(prefix):
    lastnonvirt = 0 
    for i in xrange(len(prefix)):
        if 'virtual' in prefix[i].specs:
            break
        lastnonvirt += 1
    return lastnonvirt


# evaluate all dependencies present in test b comparing them to the dpeendencies in gold
# returns for every word in gold whether it was included at all and wether it was attached correctly
# there are three arrays, called unspecified, correct-unlabeled, correct-labled
# the return value [[0,0,1],[1,0,0],[1,0,0]] would indicate 
# the first word was attached correctly (both labeled and unlabeled), 
# the attachment for the second word was wrong and the third word wasn't included at all
# (TODO there are more elements than that in the ouput now)
def evalDependencies(test, gold, virtmap):
    if verbose:
        print "========================"
        print " test: "
        printAnno(test)
        print "------------------------"
        print "gold: "
        printAnno(gold)

    # first get the index of the last non-virtual node
    lastnonvirt = getLastNonVirt(test)

    unspec = [1]*len(gold)
    cor_l  = [0]*len(gold)
    cor_u  = [0]*len(gold)
    nspec  = [0]*len(gold)
    ns_c_l = [0]*len(gold)
    ns_c_u = [0]*len(gold)
    
    strPred= [0]*4 # (count , cor_u, cor_l, no_map)

    # --------------------------------------
    # calculate accuracy scores 
    # by iterating through all tokens 
    # in test
    # first the nonvirtual ones
    for n in xrange(lastnonvirt):
        tword = test[n]
        tp= tword.specs["SYN"].pointer
        tv= tword.specs["SYN"].value
        gword = gold[n]
        wnum= n+1

        try:
            unspec[n] = 0

            # regent = nonspec
            if tp <= -1:
                if structural:
                    #handle nonspec as one single virtual node
                    if virtmap[tp] == gword.specs["SYN"].pointer:
                        cor_u[n] = 1
                        if tv == gword.specs["SYN"].value:
                            cor_l[n] = 1

                else:
                    # handle nonspec as generic future node
                    nspec[n] += 1
                    if gword.specs["SYN"].pointer > lastnonvirt:
                        cor_u[n] += 1
                        ns_c_u[n] += 1
                        if tv == gword.specs["SYN"].value:
                            cor_l[n] += 1
                            ns_c_l[n] += 1
                    #else:
                    #    print tword.word + "--" + tv+ "->" + str(tp)
                    #    print " l " + str(len(test))

            # regent = virtual node
            elif tp > lastnonvirt:
                # handle as generic future node, like nonspec
                if structural:
                # handle as specific virtual node
                    if verbose:
                        sys.stderr.write("--------------------------------------\n")
                        sys.stderr.write("known to virtual node\n")
                        sys.stderr.write("test  :"+ wnum+" --"+ tv+ "-->"+ tp+ "\n")
                        sys.stderr.write("mapped:"+ wnum," --"+ tv+ "-->"+ virtmap[tp]+ "\n")
                        sys.stderr.write("gold  :"+ wnum+" --"+ gword.specs["SYN"].value+ "-->"+ gword.specs["SYN"].pointer+ "\n")
                    nspec[n] += 1
                    if virtmap[tp] == gword.specs["SYN"].pointer:
                        cor_u[n] += 1
                        ns_c_u[n] += 1
                        if tv == gword.specs["SYN"].value:
                            cor_l[n] += 1
                            ns_c_l[n] += 1
                else:
                    nspec[n] += 1
                    if gword.specs["SYN"].pointer > lastnonvirt:
                        cor_u[n] += 1
                        ns_c_u[n] += 1
                        if tv == gword.specs["SYN"].value:
                            cor_l[n] += 1
                            ns_c_l[n] += 1
                        
            # regent known
            else:
                if tp == gword.specs["SYN"].pointer:
                    cor_u[n] += 1
                    if tv == gword.specs["SYN"].value:
                        cor_l[n] += 1
                
        except:
            continue # something is wrong with this entry, just ignore it        
    # end for  wnum in xrange(lastnonvirt+1)    

    if structural:
        #now iterate over virtual nodes, if any
        for n in xrange(lastnonvirt,len(test)):
            tword= test[n]
            wnum= n+1
            tp= tword.specs["SYN"].pointer
            tv= tword.specs["SYN"].value

            # skip unused nodes
            if tp == 0 and tv == "":
                continue

            # treat this edge as a virtual one, no matter the regent
            
            # count structurally predicted nodes seperately
            strPred[0]+=1

            try:
                if(wnum in virtmap.keys()) and virtmap[wnum] != -1:
                    gword= gold[virtmap[wnum]-1]
                    gp= gword.specs["SYN"].pointer
                    gv= gword.specs["SYN"].value
                    unspec[virtmap[wnum]-1] = 0
                else:
                    # there is no mapping for this node
                    strPred[3]+=1
                    continue # counted in cnt, but not as correct occurence

                # regent == nonspec
                if tp <= -1:
                    # handle nonspec as one single virtual node
                    nspec[virtmap[wnum]-1] += 1
                    if virtmap[tp] == gp:
                        cor_u[virtmap[wnum]-1] += 1
                        ns_c_u[virtmap[wnum]-1] += 1
                        strPred[1]+=1
                        if tv == gv:
                            cor_l[virtmap[wnum]-1] += 1
                            ns_c_l[virtmap[wnum]-1] += 1
                            strPred[2]+=1
                        
                # regent = also a virtual node
                elif tp > lastnonvirt:
                    if verbose:
                        sys.stderr.write("--------------------------------------\n")
                        sys.stderr.write("virtual to virtual node\n")
                        sys.stderr.write("test  : "+ wnum+"--"+ tv+ "-->"+ tp+ "\n")
                        sys.stderr.write("mapped: "+ virtmap[wnum]+"--"+ tv+ "-->"+ virtmap[tp]+ "\n")
                        sys.stderr.write("gold  : "+ virtmap[wnum]+"--"+ gv+ "-->"+ gp+ "\n")

                    # handle as specific virtual node
                    nspec[virtmap[wnum]-1] += 1
                    if tp in virtmap.keys() and virtmap[tp] == gp:
                        cor_u[virtmap[wnum]-1] += 1
                        ns_c_u[virtmap[wnum]-1] += 1
                        strPred[1]+=1
                        if tv == gv:
                            cor_l[virtmap[wnum]-1] += 1
                            ns_c_l[virtmap[wnum]-1] += 1
                            strPred[2]+=1
        
                # regent known
                else:
                    if verbose:
                        sys.stderr.write("--------------------------------------\n")
                        sys.stderr.write("virtual to known node\n")
                        sys.stderr.write("test  : "+ wnum+"--"+ tv+ "-->"+ tp+ "\n")
                        sys.stderr.write("mapped: "+ virtmap[wnum]+"--"+ tv+ "-->"+ tp+ "\n")
                        sys.stderr.write("gold  : "+ virtmap[wnum]+"--"+ gv+ "-->"+ gp+ "\n")

                    nspec[virtmap[wnum]-1] += 1
                    if tp == gp:
                        cor_u[virtmap[wnum]-1] += 1
                        ns_c_u[virtmap[wnum]-1] += 1
                        strPred[1]+=1
                        if tv == gv:
                            cor_l[virtmap[wnum]-1] += 1
                            ns_c_l[virtmap[wnum]-1] += 1
                            strPred[2]+=1
            except:
                sys.stderr.write("ERROR: something wrong with word"+ tword.word + ", skipping...\n")
                continue # something is wrong with this entry, just ignore it
        # end for 
    # end if structural

    return [unspec, cor_u, cor_l, nspec, ns_c_u, ns_c_l, strPred]
# end def evalDependencies


# evaluates all prefixe parses for a sentence and returns a comprehensive analysis
# returns an array [total, unspec, correct_u, correct_l, final]
def evalSentence(allPrefixes, gold, nback, nforw):
    total     = [0]*( 1 + nback + nforw )
    unspec    = [0]*( 1 + nback + nforw )
    correct_u = [0]*( 1 + nback + nforw )
    correct_l = [0]*( 1 + nback + nforw )
    stable_u  = [0]*( 1 + nback + nforw )
    stable_l  = [0]*( 1 + nback + nforw )
    nspec     = [0]*( 1 + nback + nforw )
    ns_c_u    = [0]*( 1 + nback + nforw )
    ns_c_l    = [0]*( 1 + nback + nforw )
    
    #structural Prediction
    strPred   = [0]*4 # (count, cor_u, cor_l, no_map)

    n = len(gold)

    if malt:
        if n != len(allPrefixes)-1:
            sys.stderr.write("WARNING: test and gold not of equal length: " + str(len(allPrefixes)-1) + " vs " + str(n)+ "\n")
    else:
        if n != len(allPrefixes):
            sys.stderr.write("WARNING: test and gold not of equal length: " + str(len(allPrefixes)-1) + " vs " + str(n)+ "\n")

    # for margin and lookahead handling as well as malt specific requirements, we have to manipulate the list of prefixes
    if malt:
        prefixes = preparePrefixList(allPrefixes[:-2], allPrefixes[-1], nback, nforw, la)
    else:
        prefixes = preparePrefixList(allPrefixes[:-1], allPrefixes[-1], nback, nforw, la)


    if margin:
        i= -nforw
    else:
        i=0

    results = []

    goldRoots = countRoots(gold)
    roots= 0
    rootsDistri= [0]*11

    for prefix in prefixes:
        prepareNonspec(prefix)
        if structural:
            virtmap = bestVirtMap(prefix, gold)
        else:
            virtmap = None        
        results   = evalDependencies(prefix,   gold,            virtmap) 
        stability = evalDependencies(prefix, allPrefixes[-1], virtmap) 
        
        r = countRoots(prefix)
        roots += r
        if r >= len(rootsDistri):
            r= len(rootsDistri)-1
        rootsDistri[r] += 1

        for j in xrange(-nback, nforw+1):
            k = i + j
            #k= i + j +la # + la, because, with lookahead 
                          # the input is actually further along than i
            #if i+la >= n: # don't delay the words at the right 
                           # end of the sentence
            #    k-= i+la - (n-1)
            
            if k >= 0 and k < n:
                total[j+nback]    += 1
                unspec[j+nback]   += results[0][k]
                correct_u[j+nback]+= results[1][k]
                correct_l[j+nback]+= results[2][k]
                stable_u[j+nback] += stability[1][k]
                stable_l[j+nback] += stability[2][k]
                nspec[j+nback]+= results[3][k]
                ns_c_u[j+nback]+= results[4][k]
                ns_c_l[j+nback]+= results[5][k]
                
        for j in xrange(4):
            strPred[j]+= results[6][j]
        
        i+=1
    # end for all prefixes

    final_t   = len(gold)
    final_c_u = reduce((lambda x, y: x+y), results[1])
    final_c_l = reduce((lambda x, y: x+y), results[2])
    final_ns  = reduce((lambda x, y: x+y), results[3])
    final = [final_t, final_c_u, final_c_l, final_ns]
    
    root = [goldRoots, len(prefixes), roots, rootsDistri]
    
    return [total, unspec, correct_u, correct_l, stable_u, stable_l, 
            nspec, ns_c_u, ns_c_l, final, root, strPred]

# returns a gold parse, read from a file identified by pattern and id
def readGold(gPattern, id):
    #print "pattern " + gPattern
    name= gPattern.replace("(1)", str(id))
    #print "opening " + name
    return parser.parse(unicode(open( name ).read(), 'latin-1'))

# reads all prefixe parses of a sentence
# returns a list of parses
def readTest(tPattern, id, n):
    test=[]  

    # malt has an extra final result, so we add 1 to n
    if malt:
        nn = n+1
    else :
        nn = n

    # malts start at 1 not at zero, so we add 1 to i
    for i in xrange(nn):
        #if malt:
        ii = i+1 # jwcdg starts at 1 as well
        #else :
        #    ii = i

        file  = open( tPattern.replace("(1)",str(id)).replace("(2)", str(ii)) )
        coded = unicode(file.read(), 'latin-1')
        parsed = parser.parse(coded)
        test.append( parsed )

    return test



def evaluateAll(tPattern, gPattern, minId, maxId, nback, nforw):

    results= []
    d = 1 + nback + nforw

    for i in xrange(9):    # 0-8, total unspec cor_u cor_l stab_u stab_l nonspec ns_c_u ns_c_l
        results.append( [0]*d ) 
    results.append([0]*4)  #  9, final result
    results.append([0]*3)  # 10, connectedness
    results.append([0]*11) # 11, root/fragment counting
    results.append([0]*4)  # 12, structural predictions

    for id in xrange(minId, maxId+1):
        sys.stderr.write("reading sentence " + str(id)+ "\n")

        try:
            gold = readGold(gPattern, id)
        except:
            sys.stderr.write("WARNING: could not read gold file, skipping...\n")
            continue

        #print "len: %d"%(len(gold))
        try:
            n = len(gold)
            test= readTest(tPattern, id, n)
        except:
            sys.stderr.write("WARNING: could not read result file, skipping...\n")
            continue

        try: 
            rs = evalSentence(test, gold, nback, nforw)
        except:
            sys.stderr.write("WARNING: files out of sync\n")
            continue
        
        # read results
        for i in xrange(9):
            for j in xrange(d):
                results[i][j] += rs[i][j]

        # final results
        for j in xrange(4):
            results[9][j] += rs[9][j]

        # connectedness
        for j in xrange(3):
            results[10][j] += rs[10][j]
        for j in xrange(len(results[11])):
            results[11][j] += rs[10][3][j]
           
        # structural predictions
        for j in xrange(4):
            results[12][j] += rs[11][j]

    return results




tPattern = args[0]
gPattern = args[1]
minId    = int(args[2])
maxId    = int(args[3])
nback    = int(args[4])
nforw   = int(args[5])
        
results= evaluateAll(tPattern, gPattern, minId, maxId, nback, nforw)

# print ""
print "# connectedness:"
print "# gold: %f"%(results[10][0]/float(maxId-minId)) 
print "# test: %f"%(results[10][2]/float(results[10][1])) 
s= "distri: "
for i in xrange(len(results[11])):
    s+= "%d,"%(results[11][i])
print "#", s
print "#"

print "# id total unspec cor_u cor_l stab_u stab_l nonspec ns_c_u ns_c_l"
print "pred-prec %d 0 %d %d 0 0 %d %d %d %d %d %d %d"%(results[12][0], results[12][1], results[12][2], results[12][0], results[12][1], results[12][2],results[12][0], results[12][1], results[12][2], results[12][3])

for i in  xrange(-nforw, nback+1):
    ii = -i + nback
    print "%d %d %d %d %d %d %d %d %d %d 0 0 0 0" % (i, results[0][ii], results[1][ii], results[2][ii], 
                                             results[3][ii], results[4][ii], results[5][ii],
                                             results[6][ii], results[7][ii], results[8][ii])
#print "# final"
print "%s %d %d %d %d %d %d %d 0 0" % ('final', results[9][0], 0, results[9][1], 
                             results[9][2], results[9][0], results[9][0], results[9][3])
