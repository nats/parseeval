#! /usr/bin/env python
# -*- coding:utf-8 -*-
# An evaluator for cda files without good documentation
# Copyright (C) 2010  Niels Beuck
# Copyright (C) 2010  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from cda_parse.cdgoutparse import parser, CdaSpec, CdaWord, ParseException

import sys
import copy

from optparse import OptionParser
optparser = OptionParser()
optparser.add_option("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="print infos about mappings etc.")

(options, args) = optparser.parse_args()
verbose = options.verbose

if len(args) == 0:
    print """usage: cdgevaluator.py [test] [gold] ..."""
    exit
if (len(args)) % 2:
    print """you must specify an even number of files!"""
    exit

def better(score1, score2):
    if score1[0] < score2[0] or ( score1[0] == score2[0] and score1[1] < score2[1] ):
        return True
    return False

def printAnno(anno):
    i=1
    for word in anno:
        print i, word.word, "--", word.specs["SYN"].value,"->", word.specs["SYN"].pointer
        i+=1

def evalVM(vm, test, gold):
    #print "==============="
    #print "called: evalVM"
    #print "vm: ", vm
    errors= [0,0]
    for n in xrange(len(test)):
        tword= test[n]
        wnum =n+1 # off by one between array indices and cdg counting scheme
        tp= tword.specs["SYN"].pointer
        tv= tword.specs["SYN"].value
        
        if wnum in vm.keys():
            # dependent virtual
            
            if vm[wnum] == -1:
                # cannot be mapped                    
                errors[0] += 1
                errors[1] += 1
                continue
            
            else:
                gword= gold[vm[wnum]-1] # wnum instead of n, as we first have to look it up, 
                                        # then subtract 1
            
        elif "virtual" in tword.specs:
            # virtual but not in virtmap, thus unused, skip it
            continue
        else:
            # print "n", n
            gword= gold[n]

        gp= gword.specs["SYN"].pointer
        gv= gword.specs["SYN"].value
        if tp in vm.keys():
            # regent virtual
            
            if vm[tp] == -1:
                # cannot be mapped
                errors[0]+=1
                errors[1]+=1
                continue
                    
            else:
                # can be mapped, map it
                tp= vm[tp]

        # independent of whether it has been mapped or not, compare it
        if gp == tp:
            if not gv == tv:
                # label is wrong
                errors[0]+=1
            # else: all is well
        else:
            # attachment is wrong
            errors[0]+=1
            errors[1]+=1

    # end for n in xrange...
    
    return errors                    
# end def evalVM


# 
def evalDependencies(test, gold):
    if verbose:
        print "========================"
    #print "called: evalDependencies"
    #print "------------------------"
    if verbose:
        print " test: "
        printAnno(test)
        print "------------------------"
        print "gold: "
        printAnno(gold)

    # [count, labeled, unlabeled, base]
    # constants:
    cnt=0 # occurences in test
    lbl=1 # correct label and regent in test
    ulb=2 # correct regent in test
    bas=3 # base, occurences in gold

    total       = [0,0,0,0] # words in the analysis

    known       = [0,0,0,0] # words with known regents
    future      = [0,0,0,0] # words with future regents
    virtual     = [0,0,0,0] # virtual dependents

    # first get the index of the last non-virtual node
    lastnonvirt = 0 # in cdg space, where 0 = nil, not array space
    for i in xrange(len(test)):
        if 'virtual' in test[i].specs:
            break
        lastnonvirt += 1
    # lastnonvirt -= 1  # not needed, off by one error compensates
    if verbose:
        print "------------------------"
        print "lastnonvirt: ", lastnonvirt

    # calculate optimal mapping from virtual nodes to upcoming words
    virtmap= {}
    candidates = {}
    
    # initialize candidates
    #for n in xrange(lastnonvirt,len(test)):
    #    wnum= n+1
    #    candidates[wnum] = []
    #candidates[-1]= []
 

    # collect candidates
    # virtual node as regent
    for n in xrange(lastnonvirt):
        tword= test[n]
        if n >= len(gold):
            sys.stderr.write("WARNING: word "+tword.word+ " not in annotation\n")
        gword= gold[n]
        gp= gword.specs["SYN"].pointer
        wnum= n+1
        tp= tword.specs["SYN"].pointer 
        #print "tp:", tp, "len(candidates):", len(candidates),"candidates[tp]:", candidates[tp]
        #print "gword:", gword, "gwordpointer:", gword.specs["SYN"].pointer
        if tp > lastnonvirt or tp == -1 :
            if not tp in candidates.keys():
                candidates[tp]= [-1]
            if gp> lastnonvirt :
                candidates[tp].append(gp)

    # virtual node as dependent
    for n in xrange(lastnonvirt,len(test)):
        tword= test[n]
        wnum= n+1
        tp = tword.specs["SYN"].pointer
        if tp <= lastnonvirt and (tp != 0 or tword.specs["SYN"].value != ""):
            #print wnum, "--", tword.specs["SYN"].value,"-> ", tp 
            # check possible dependents
            for m in xrange(lastnonvirt, len(gold)):
                gword= gold[m]
                wnum2= m+1
                if gword.specs["SYN"].pointer == tword.specs["SYN"].pointer:
                    if not wnum in candidates.keys():
                        candidates[wnum]= [-1]
                    if wnum2> lastnonvirt :
                        candidates[wnum].append(wnum2)

    # handle purely virtual dependents
    for n in xrange(lastnonvirt,len(test)):
        tword= test[n]
        wnum= n+1
        tp= tword.specs["SYN"].pointer
        if tp > lastnonvirt or tp == -1 :
            if not tp in candidates.keys():
                candidates[tp]= [-1]
            if not wnum in candidates.keys():
                candidates[wnum]= [-1]
                # no use to go on, as there are no candidates, continue 
                continue
                
            # add new candidates for the regent
            for cand in candidates[wnum]:
                if cand != -1:
                    cword = gold[cand-1]
                    cp = cword.specs["SYN"].pointer
                    if cp > lastnonvirt and not cp in candidates[tp]:
                        candidates[tp].append(cp)
            
            # for all candidates of its regent
            for cand in candidates[tp]:
                for m in xrange(lastnonvirt,len(gold)):
                    gword= gold[m]
                    wnum2= m+1
                    if gword.specs["SYN"].pointer == cand:
                        if not wnum in candidates.keys():
                            candidates[wnum]= [-1]
                        if wnum2 > lastnonvirt:
                            candidates[wnum].append(wnum2)

    if verbose:
        print "candidates: ", candidates

    # now consolidate the candidates to one interpretation

    # first simply make sets
    for key in candidates.keys():
        candidates[key]= list(set(candidates[key]))

    # we need to know if there are contested target nodes
    targets= {}
    for key in candidates.keys():
        for cand in candidates[key]:
            if not cand in targets.keys():
                targets[cand]= []
            targets[cand].append(key)

    for key in targets.keys():
        targets[key]= list(set(targets[key]))
        
    # assign all unique matchings
    remaining={}
    for key in candidates.keys():
        if len(candidates[key]) == 1 and len(targets[candidates[key][0]]) == 1 : 
            virtmap[key]= candidates[key][0]
        else:
            remaining[key]= candidates[key]
            
    # now let the fun part begin: try out all combinations and keep the best one
    # [0: vmc, 1: list of occupied candidates]
    #vmcs = map(lambda x: [x, []],copy.copy(virtmap))
    vmcs = [[copy.copy(virtmap), []]]
    for key in remaining.keys():
        new_vmcs = []
        # print "vmcs: ", vmcs
        for vmc in vmcs:
            # print key, "from", remaining
            for cand in remaining[key]:
                #print "cand:", cand
                #print "vmc[1]:", vmc[1]
                #print "vmc:", vmc
                if not(cand in vmc[1]):
                    new_vmc= copy.copy(vmc[0])
                    new_vmc[key]= cand
                    new_vmc1=copy.copy(vmc[1])
                    new_vmc1.append(cand)
                    new_vmcs.append([ new_vmc, new_vmc1 ])
            new_vmc= copy.copy(vmc[0])
            # print "key:", key, "new_vmc:", new_vmc
            new_vmc[key]= -1 # no mapping
            new_vmcs.append([ new_vmc , copy.copy(vmc[1]) ])
        vmcs = new_vmcs

    # search for the best one
    best = [1000, 1000] # [labelled, unlabelled]
    best_vm = {}
    if(len(vmcs) == 0):
        sys.stderr.write("ERROR, no mapping found!\n")
        # TODO: What happens after this error?
    for vmcl in vmcs:
        vmc= vmcl[0]
        score= evalVM(vmc, test, gold)
        if better(score, best):
            best_vm= vmc
            best=score

    virtmap= best_vm
    if verbose:
        print "selected mapping: ", virtmap
        
    # --------------------------------------
    # calcualte accuracy scores 
    # by iterating through all tokens 
    # in test
    # first the nonvirtual ones
    for n in xrange(lastnonvirt):
        tword = test[n]
        tp= tword.specs["SYN"].pointer
        tv= tword.specs["SYN"].value
        gword = gold[n]
        wnum= n+1

        # count everything we can find
        total[cnt] +=1

        try:
            # regent = nonspec
            if tp == -1:
                # handle nonspec as generic future node
                future[cnt] += 1 
                if gword.specs["SYN"].pointer > lastnonvirt:
                    future[ulb] += 1
                    if tv == gword.specs["SYN"].value:
                        future[lbl] += 1
                # handle nonspec as one single virtual node
                virtual[cnt] += 1 
                # print "NONSPEC mapped to: ", virtmap[-1]
                if virtmap[-1] == gword.specs["SYN"].pointer:
                    virtual[ulb] += 1
                    if tv == gword.specs["SYN"].value:
                        virtual[lbl] += 1
                        
                        
            # regent = virtual node
            elif tp > lastnonvirt:
                # handle as generic future node, like nonspec
                future[cnt]  += 1 
                if gword.specs["SYN"].pointer > lastnonvirt:
                    future[ulb] += 1
                    if tv == gword.specs["SYN"].value:
                        future[lbl] += 1
                        
                # handle as specific virtual node
                virtual[cnt] += 1
                if verbose:
                    print "--------------------------------------"
                    print "known to virtual node"
                    print "test  :", wnum," --", tv, "-->", tp
                    print "mapped:", wnum," --", tv, "-->", virtmap[tp]
                    print "gold  :", wnum," --", gword.specs["SYN"].value, "-->", gword.specs["SYN"].pointer
                if virtmap[tp] == gword.specs["SYN"].pointer:
                    virtual[ulb] += 1
                    if tv == gword.specs["SYN"].value:
                        virtual[lbl] += 1
                        
            # regent known
            else:
                known[cnt] += 1
                if tp == gword.specs["SYN"].pointer:
                    known[ulb] += 1
                    if tv == gword.specs["SYN"].value:
                        known[lbl] += 1
        except:
            continue # something is wrong with this entry, just ignore it
        
        
        
        #        if tword.word != gword.word: # sanity check - re-enable if cdg gets sane ;-)
        #            exit(tword.word+" doesn't match "+gword.word)

    # end for  wnum in xrange(lastnonvirt+1)    

    #now iterate over virtual nodes, if any
    for n in xrange(lastnonvirt,len(test)):
        tword= test[n]
        wnum= n+1
        tp= tword.specs["SYN"].pointer
        tv= tword.specs["SYN"].value

        # skip unused nodes
        if tp == 0 and tv == "":
            continue

        # count this edge as a virtual one, no matter the regent
        virtual[cnt] += 1 

        try:
            if(wnum in virtmap.keys()) and virtmap[wnum] != -1:
                gword= gold[virtmap[wnum]-1]
                gp= gword.specs["SYN"].pointer
                gv= gword.specs["SYN"].value
            else:
                # there is no mapping for this node
                continue # counted int cnt, but not as correct occurence

            # regent == nonspec
            if tp == -1:
                # handle nonspec as one single virtual node
                
                if virtmap[-1] == gp:
                    virtual[ulb] += 1
                    if tv == gv:
                        virtual[lbl] += 1
 
                        
            # regent = also a virtual node
            elif tp > lastnonvirt:
                if verbose:
                    print "--------------------------------------"
                    print "virtual to virtual node"
                    print "test  : ", wnum,"--", tv, "-->", tp
                    print "mapped: ", virtmap[wnum],"--", tv, "-->", virtmap[tp]
                    print "gold  : ", virtmap[wnum],"--", gv, "-->", gp

                # handle as specific virtual node
                if tp in virtmap.keys() and virtmap[tp] == gp:
                    virtual[ulb] += 1
                    if tv == gv:
                        virtual[lbl] += 1
        
            # regent known
            else:
                if verbose:
                    print "--------------------------------------"
                    print "virtual to known node"
                    print "test  : ", wnum,"--", tv, "-->", tp
                    print "mapped: ", virtmap[wnum],"--", tv, "-->", tp
                    print "gold  : ", virtmap[wnum],"--", gv, "-->", gp

                if tp == gp:
                    virtual[ulb] += 1
                    if tv == gv:
                        virtual[lbl] += 1
        except:
            sys.stderr.write("ERROR: something wrong with word"+ tword.word+ ", skipping...\n")
            continue # something is wrong with this entry, just ignore it
    # end for  wnum in xrange(lastnonvirt+1,len(test))


    # count how many opportunities for future and virtual regents there where,
    # needed for the recall score
    for n in xrange(len(gold)):
        gword = gold[n]
        gp= gword.specs["SYN"].pointer
        gv= gword.specs["SYN"].value
        wnum= n+1

        total[bas] += 1

        if wnum > lastnonvirt:
            # the word itself is outside of the prefix, 
            # the edge can only be captured by virtuallity
            # don't regard punctuation and such
            if gp == 0 and gv =='':
                continue # skip it
            else:
                virtual[bas] += 1 
            # TODO maybe differentiate purly virtual nodes
        elif gp > lastnonvirt:
            # the regent is outside the prefix, nonspec or virtual nodes could handle this.
            future[bas]  += 1
            virtual[bas] += 1
        else:
            # ordinary edge, both ends are in the prefix
            known[bas] +=1
    # end for wnum in xrange(len(gold)):
        
    print args[x*2]

    print "increment", lastnonvirt-1 # array space
    print "knownl", known[lbl]
    print "knownu", known[ulb]
    print "knowncnt", known[cnt]
    print "knownbase", known[bas]

    print "futurel", future[lbl]
    print "futureu", future[ulb]
    print "futurecnt", future[cnt]
    print "futurebase", future[bas]

    print "virtuall", virtual[lbl]
    print "virtualu", virtual[ulb]
    print "virtualcnt", virtual[cnt]
    print "virtualbase", virtual[bas]
    print

# end def evalDependencies    

# lets start: iterate over test and compare it to gold
for x in xrange(len(args)/2):   
    # I'm too lazy to catch errors. Just explode.
    test = parser.parse(unicode(open(args[x*2]).read(), 'latin-1'))
    gold = parser.parse(unicode(open(args[x*2+1]).read(), 'latin-1'))

    evalDependencies(test, gold)


