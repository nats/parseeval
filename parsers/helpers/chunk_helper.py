#! /usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import subprocess
from ConfigParser import ConfigParser

import re
wordre = re.compile(r"'(?:[^\\']|\\.)*'|[\w§]+")

config = ConfigParser()
mydir = os.path.abspath(os.path.dirname(sys.argv[0]))
config.read(mydir+"/chunk_helper.conf")

sentence = re.findall(wordre, sys.stdin.readline().strip())

sentence = [x.strip("'") for x in sentence]

sp = subprocess.Popen([config.get('tagger','executable'),
                       "-c", "incrementalnoprefix",
                       config.get('tagger','model')],
                      stdin=subprocess.PIPE,
                      stdout=subprocess.PIPE)

sp.stdin.write("\n".join(sentence))
sp.stdin.close()
res = sp.stdout.readlines()
first = True
words = []
prevchunk = []
for i in res:
    try:
        word,tag = i.rsplit("\t")[0:2]
    except:
        sys.stderr.write("ERRORERROR: "+i)
        raise Exception()
    words.append("'"+word+"'")
    if tag == "yes":
        if not prevchunk: # we have no chunk ready
            prevchunk = words
            words = []
            continue
        if first:
            print "inputwordgraph", " ".join(prevchunk)
            print "incremental chunked"
            first = False
        else:
            print "incremental -c", " ".join(prevchunk)
        prevchunk = words
        words = []
prevchunk.extend(words)
if first: # the whole sentence is one chunk
    print "set autofinalize on"
    print "inputwordgraph", " ".join(prevchunk)
    print "incremental chunked"
else:
    print "incremental -cf", " ".join(prevchunk)
