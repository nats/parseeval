#! /usr/bin/env python
# -*- coding: utf-8 -*-

# reads: a sentence graph; output: for each word whether the number of
# edges going to the right of this word has not increased.  purpose:
# we can train a tagger with this and chunk sentences based on the
# output of the tagger.

# Copyright (C) 2010  Arne Köhn <arne@arne-koehn.de>
# GPLv3 or later

import sys, os
cdapath = os.path.abspath(os.path.dirname(sys.argv[0]))+'/../../cda_eval/cda_parse/'
sys.path.append(cdapath)
from cdgoutparse import parser

import sys

def doit(sentence):
    position = 1
    openwords = set()
    numowold = 0 # number of words that pointed to the future in the last run
    temp = set()
    for word in sentence:
        for ow in openwords:
            if ow.specs["SYN"].pointer == position:
                temp.add(ow)
        openwords.difference_update(temp)
        temp.clear()
        if word.specs["SYN"].pointer > position:
            openwords.add(word)

        print word.word.encode('latin-1'), # len(openwords),
        if len(openwords)<=numowold:
            # some prev. open words have been bound
            print "yes"
        else:
            print "no"
        position += 1
        numowold = len(openwords)

if __name__ == '__main__':
    for arg in sys.argv[1:]:
        sentence = parser.parse(unicode(open(arg).read(), 'latin-1'))
        doit(sentence)
        print

