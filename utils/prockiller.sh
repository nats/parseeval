#! /bin/sh
# this amount of memory should stay unallocated (in kb)
# set to 1.5gb, because we don't want to start to swap
freeneeded=1500000
user=`whoami`
while true; do
    memfree=`cat /proc/meminfo | grep MemFree | sed 's/.* \([0-9]*\) kB/\1/'`
    # echo `hostname` : free memory: $memfree
    if [  $freeneeded -ge $memfree ]; then
	echo "no memory"
	proctokill=`ps -U $user -o pid --sort=-size | head -n 2 | tail -n 1`
	kill -9 $proctokill && echo killed $proctokill on `hostname`
    fi
    sleep 3
done
