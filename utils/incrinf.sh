#! /bin/bash

for ((i=1; i<500; i++))
do
    SENTENCEFILE=$(sqlite3 ../database.db "SELECT filename from sentences WHERE id=$i;")
    ./incrementinformation.py $SENTENCEFILE | sed "s/\([0-9]*\) \([0-9]*\) \([0-9]*\) \([0-9]*\)/INSERT INTO incr_info (sentence, increment, numcrossingedges, numopenedges, numvirt) VALUES ($i, \1, \3, \2, \4);/" | sqlite3 ../database.db

done
