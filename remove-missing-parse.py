#! /usr/bin/env python
# -*- coding:utf-8 -*-
# author: Arne Köhn (arne@arne-koehn.de)
# license: GPLv3 (see LICENSE)

# This script searches for records that are in the database without a
# corresponding real result in the filesystem.  This is useful if the
# parser crashed and we want to re-run it to get results.

from os import popen
import re

cdare = re.compile("^\d+-.*cda")

outdir = popen('grep "^output_directory"  train-evaluate.conf | sed "s/.*=\(\.*\)/\1/"').read().strip()
print "outdir:", outdir
sentenceids = [x.strip() for x in popen('sqlite3 database.db "select id from sentences;"').readlines()]
parsers =  [x.strip() for x in popen('sqlite3 database.db "select distinct parser from parsed;"').readlines()]

outfiles = [x.strip() for x in popen("ls "+ outdir).readlines()]
# now remove the last part, so xsa3that only "sentenceid-parser" remains. Kick out all uninteresting files.
# create a set to make searching faster
outfiles = set([x.rsplit("-",1)[0] for x in filter(cdare.match, outfiles)])

for p in parsers:
    for s in sentenceids:
        if not s+"-"+p  in outfiles:
            print s+"-"+p, "not found"
            popen('sqlite3 database.db "delete from parsed where sentence=%s and parser=\'%s\';"'%(s,p))
            popen('sqlite3 database.db "delete from results where sentence=%s and parser=\'%s\';"'%(s,p))
