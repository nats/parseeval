#! /usr/bin/env perl
# license : gplv2 or later (see LICENSE)
# author: Arne Köhn <arne@arne-koehn.de>
use strict;

package wpreader;

# example output that has to be parsed:
# token: 0 - der alte Mann 
# increment: 3
# time: 3180
# score: 0.938
# steps: 10
# patches: 5 / 6 
# evals: 4676036 - 1168564 
# amounts_LV-Lex: 3268 - 54 
# unremoved:virtueller Regent((4/SUBJ/14)())
# repairattempts:
# y : 0.164 : 1 : doppeltes Objekt
# y : 0.180 : 1 : ATTR-Kette
# y : 0.212 : 1 : Determiner fehlt
# n : 0.804 : 5 : virtueller Regent
# y : 0.088 : 1 : virtueller Regent
# y : 0.080 : 1 : virtueller Regent
#     ;;

# token: 1 - ging 
# increment: 4
# time: 796
# score: 0.987
# steps: 0
# patches: 0 / 0 
# evals: 6432761 - 1199894 
# amounts_LV-Lex: 4475 - 61 
# unremoved:
# repairattempts:
#     ;;

sub read_time_information {
    # returns a list of hashes. Each hash contains the increment
    # number, needed time and whether the timelimit kicked in.

    my $wpfilename = shift;
    print STDERR $wpfilename;
    my @result = ();
    my $increment = -1;
    my $time = -1;
    my $timelimit = 0;
    my $wpfile;
    open($wpfile, '<', $wpfilename) or return 0;
    while (<$wpfile>) {
	if ( m/^time: (\d+)/ ) {
	    $time = $1;
	}
	if ( m/^timelimit: (.+)/ ) {
	    $timelimit = $1;
	}
	if ( m/^nrtokens: (\d+)/ ) {
	    $increment = $1
	}
	if ( m/^token: (\d+)/ ) {
	    if ( $increment > -1 ) { # not in first line
		push @result, {'increment'=>$increment, 'time'=>$time,
			       'timelimit'=>$timelimit}
	    }
	    $time = -1;
	    $timelimit = 0;
	}
    }
    return \@result
}

unless (caller) {
    foreach my $arg (@ARGV) {
	my $res = wpreader::read_time_information($arg);
	print "result for $arg\n";
	foreach my $t  (@$res) {
	    print "increment: ", $t->{'increment'}, "\n";
	    print "time: ", $t->{'time'}, "\n";
	    print "timelimit: ", $t->{'timelimit'}, "\n\n";
	}
    }	
}
